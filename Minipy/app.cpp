#include "app.h"

wxIMPLEMENT_APP(app);

app::app() {

}

app::~app() {

}

bool app::OnInit() {
	main_frame = new main();
	main_frame->Show();
	return true;
}

int app::OnExit() {
	//Do Application-wide cleanup here
	return 0;
}
