#include "customListBoxItem.h"

customListBoxItem::customListBoxItem(wxString p_name, wxString p_root_dir) {
	name = p_name;
	root_dir = p_root_dir;
}

customListBoxItem::~customListBoxItem() {

}

wxString customListBoxItem::get_name() {
	return name;
}

wxString customListBoxItem::get_root_dir() {
	return root_dir;
}

void customListBoxItem::set_name(wxString p_name) {
	name = p_name;
}
