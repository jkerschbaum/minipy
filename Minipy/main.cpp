#include "main.h"

enum
{
    MARGIN_LINE_NUMBERS,
    MARGIN_FOLD
};

void main::setStyle(wxStyledTextCtrl& text) {
    text.SetMarginWidth(MARGIN_LINE_NUMBERS, 30);
    text.StyleSetForeground(wxSTC_STYLE_LINENUMBER, wxColour(75, 75, 75));
    text.SetMarginType(MARGIN_LINE_NUMBERS, wxSTC_MARGIN_NUMBER);


    // ---- Enable code folding
    text.SetMarginType(MARGIN_FOLD, wxSTC_MARGIN_SYMBOL);
    text.SetMarginWidth(MARGIN_FOLD, 15);
    text.SetMarginMask(MARGIN_FOLD, wxSTC_MASK_FOLDERS);
    text.SetMarginSensitive(MARGIN_FOLD, true);

    // Properties taken from http://www.scintilla.org/SciTEDoc.html
    text.SetProperty(wxT("fold"), wxT("1"));
    text.SetProperty(wxT("fold.comment"), wxT("1"));
    text.SetProperty(wxT("fold.compact"), wxT("1"));

    //Set the colours for the markers
    //Foregroud = Lines, Background = Fill
    wxColor grey(100, 100, 100);
    text.MarkerDefine(wxSTC_MARKNUM_FOLDER, wxSTC_MARK_ARROW);
    text.MarkerSetForeground(wxSTC_MARKNUM_FOLDER, grey);
    text.MarkerSetBackground(wxSTC_MARKNUM_FOLDER, grey);

    text.MarkerDefine(wxSTC_MARKNUM_FOLDEROPEN, wxSTC_MARK_ARROWDOWN);
    text.MarkerSetForeground(wxSTC_MARKNUM_FOLDEROPEN, grey);
    text.MarkerSetBackground(wxSTC_MARKNUM_FOLDEROPEN, grey);

    text.MarkerDefine(wxSTC_MARKNUM_FOLDERSUB, wxSTC_MARK_EMPTY);
    text.MarkerSetForeground(wxSTC_MARKNUM_FOLDERSUB, grey);
    text.MarkerSetBackground(wxSTC_MARKNUM_FOLDERSUB, grey);

    text.MarkerDefine(wxSTC_MARKNUM_FOLDEREND, wxSTC_MARK_ARROW);
    text.MarkerSetForeground(wxSTC_MARKNUM_FOLDEREND, grey);
    text.MarkerSetBackground(wxSTC_MARKNUM_FOLDEREND, _T("WHITE"));

    text.MarkerDefine(wxSTC_MARKNUM_FOLDEROPENMID, wxSTC_MARK_ARROWDOWN);
    text.MarkerSetForeground(wxSTC_MARKNUM_FOLDEROPENMID, grey);
    text.MarkerSetBackground(wxSTC_MARKNUM_FOLDEROPENMID, _T("WHITE"));

    text.MarkerDefine(wxSTC_MARKNUM_FOLDERMIDTAIL, wxSTC_MARK_EMPTY);
    text.MarkerSetForeground(wxSTC_MARKNUM_FOLDERMIDTAIL, grey);
    text.MarkerSetBackground(wxSTC_MARKNUM_FOLDERMIDTAIL, grey);

    text.MarkerDefine(wxSTC_MARKNUM_FOLDERTAIL, wxSTC_MARK_EMPTY);
    text.MarkerSetForeground(wxSTC_MARKNUM_FOLDERTAIL, grey);
    text.MarkerSetBackground(wxSTC_MARKNUM_FOLDERTAIL, grey);
    // ---- End of code folding part

    text.SetWrapMode(wxSTC_WRAP_WORD); // other choice is wxSCI_WRAP_NONE

    //Other Style ints: 
    //_CLASSNAME, _DEFNAME, _COMMENTBLOCK don't seem to affect anything
    //_IDENTIFIER seems to affect everything
    //_OPERATOR does (), :, etc 
    text.StyleSetForeground(wxSTC_P_COMMENTLINE, wxColour(0, 180, 0));
    text.StyleSetForeground(wxSTC_P_NUMBER, wxColour(230, 230, 0));
    text.StyleSetForeground(wxSTC_P_STRING, wxColour(60, 200, 200));
    text.StyleSetForeground(wxSTC_P_CHARACTER, wxColour(0, 0, 255));
    text.StyleSetForeground(wxSTC_P_WORD, wxColour(190, 0, 190));
    text.StyleSetForeground(wxSTC_P_TRIPLE, wxColour(60, 200, 200));
    text.StyleSetForeground(wxSTC_P_TRIPLEDOUBLE, wxColour(60, 200, 200));
    text.StyleSetForeground(wxSTC_P_STRINGEOL, wxColour(120, 0, 0));
    text.StyleSetForeground(wxSTC_P_WORD2, wxColour(230, 100, 0));
    text.StyleSetForeground(wxSTC_P_DECORATOR, wxColour(200, 200, 0));

    //Bold Keyword
    text.StyleSetBold(wxSTC_P_WORD, true);
    text.StyleSetBold(wxSTC_P_WORD2, true);

    //list of keywords
    text.SetKeyWords(0, wxT("break continue elif else if except finally for pass return try while"));
    text.SetKeyWords(1, wxT("and as in is lambda not or import from"));
    //Missing: False, True, None, assert, async, await, class, def, del, global, nonlocal, raise, yiels

    text.Connect(wxEVT_STC_MARGINCLICK, wxStyledTextEventHandler(main::OnMarginClick), NULL, this);
}

void main::OnMarginClick(wxStyledTextEvent& event) {
    if (event.GetMargin() == MARGIN_FOLD)
    {
        int lineClick = code_control->LineFromPosition(event.GetPosition());
        int levelClick = code_control->GetFoldLevel(lineClick);

        if ((levelClick & wxSTC_FOLDLEVELHEADERFLAG) > 0)
        {
            code_control->ToggleFold(lineClick);
        }
    }
}

main::main() : wxFrame(nullptr, wxID_ANY, "MiniPy", wxPoint(400, 50), wxSize(1000, 1000)) {
    //Create Sizer
    sizer = new wxFlexGridSizer(2, 2, 0, 0);
	//Create Menu Bar
	menu_bar = new wxMenuBar();
	main_menu = new wxMenu();
	main_menu->Append(ID_MENU_RUN, _T("&Run"));
    main_menu->Append(ID_MENU_LOAD, _T("&Load File"));
    main_menu->Append(ID_MENU_SAVE, _T("&Save File"));
	main_menu->Append(wxID_EXIT, _T("&Exit"));
	menu_bar->Append(main_menu, _T("&Menu"));
    project_menu = new wxMenu();
    project_menu->Append(ID_MENU_OPEN_PROJ, _T("&Open Project"));
    project_menu->Append(ID_MENU_SAVE_PROJ, _T("&Save Full Project"));
    project_menu->Append(ID_MENU_CLOSE_PROJ, _T("&Close Project"));
    menu_bar->Append(project_menu, _T("&Project"));
    //Create Status Bar
    status_bar = new wxStatusBar(this, ID_STATUS);
    status_bar->SetFieldsCount(1);
    status_bar->PushStatusText("Normal Operation", 0);
    SetStatusBar(status_bar);
	//Bind IDs 1001 to wxID_EXIT to main::OnMenuEvent for given event type
	Bind(wxEVT_COMMAND_MENU_SELECTED, &main::OnMenuEvent, this, ID_MENU_RUN, wxID_EXIT);
	SetMenuBar(menu_bar);
	//Create Code Text Control
	code_control = new wxStyledTextCtrl(this, ID_STC, wxPoint(100, 0), wxSize(10, 10));
	code_control->StyleClearAll();
	code_control->SetLexer(wxSTC_LEX_PYTHON);
	setStyle(*code_control);
    //Create Tree List
    tree_list = new wxTreeListCtrl(this, ID_TREELIST, wxPoint(0, 0), wxSize(10, 10), wxTL_SINGLE | wxTL_NO_HEADER);
    tree_list->AppendColumn("");
    tree_list->AppendItem(tree_list->GetRootItem(), "<local>");
    //Create Listbox
    project_history = new wxListBox(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxArrayString(), wxLB_SINGLE | wxLB_HSCROLL | wxLB_NEEDED_SB);
    populate_project_history(true);
    Bind(wxEVT_LISTBOX_DCLICK, &main::OnListBoxEvent, this);
    //Create Text Control
    pseudo_console = new wxTextCtrl(this, wxID_ANY, "", wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE | wxHSCROLL | wxTE_DONTWRAP | wxTE_PROCESS_ENTER);
    Bind(wxEVT_TEXT_ENTER, &main::OnConsoleCommandEvent, this);
    //Set Sizer
    sizer->AddGrowableCol(0, 1);
    sizer->AddGrowableCol(1, 5);
    sizer->AddGrowableRow(0, 8);
    sizer->AddGrowableRow(1, 1);
    sizer->Add(tree_list, 1, wxALL | wxEXPAND);
    sizer->Add(code_control, 1, wxEXPAND | wxALL);
    sizer->Add(project_history, 1, wxEXPAND | wxALL);
    sizer->Add(pseudo_console, 1, wxALL | wxEXPAND);
    this->SetSizer(sizer);
    sizer->Layout();
}

main::~main() {
    delete current_proj;
    //TODO: Write project history to disk
    wxTextFile project_history_file;
    std::string appdata_path = std::getenv("APPDATA");
    appdata_path.erase(appdata_path.size() - 8);
    appdata_path += "\\Local";
    std::string minipy_subpath = "\\MiniPy";
    std::string filename = "\\hist.txt";
    wxString dir_path = appdata_path + minipy_subpath;
    wxString file_path = appdata_path + minipy_subpath + filename;
    wxDir dir;
    if (!dir.Exists(dir_path)) {
        dir.Make(dir_path);
    }
    if (!wxFileExists(file_path)) {
        wxTextFile dummy;
        dummy.Create(file_path);
    }
    project_history_file.Open(file_path);
    project_history_file.Clear();
    for (int i = 0; i < (int)lbi.size(); i++) {
        project_history_file.AddLine(lbi[i].get_name());
        project_history_file.AddLine(lbi[i].get_root_dir());
    }
    project_history_file.Write();
    project_history_file.Close();
}

void main::OnConsoleCommandEvent(wxCommandEvent& evt) {
    pseudo_console->AppendText("\n");
    int num_lines = pseudo_console->GetNumberOfLines();
    wxString text = pseudo_console->GetLineText(num_lines-2);
    text = text.Upper();
    if (text == "CMD.RUN") { //Run Event
        wxCommandEvent run_event(wxEVT_COMMAND_MENU_SELECTED, ID_MENU_RUN);
        OnMenuEvent(run_event);
        return;
    }
    else if (text == "CMD.CLEAR") {
        code_control->ClearAll();
        return;
    }
    else if (text == "CMD.EXIT") {
        wxCommandEvent exit_event(wxEVT_COMMAND_MENU_SELECTED, wxID_EXIT);
        OnMenuEvent(exit_event);
    }//TODO: More commands
    pseudo_console->AppendText("Command not recognised\n");
    evt.Skip();
}

void main::OnListBoxEvent(wxCommandEvent& evt) {
    if (current_proj != nullptr) {
        wxCommandEvent close_event;
        close_event.SetId(ID_MENU_CLOSE_PROJ);
        menu_bar->GetEventHandler()->ProcessEvent(close_event);
    }
    wxString name = evt.GetString();
    wxString dir;
    for (int i = 0; i < (int)lbi.size(); i++) {
        if (name == lbi[i].get_name()) {
            dir = lbi[i].get_root_dir();
            break;
        }
    }
    current_proj = new proj(this, tree_list, code_control, true, dir);
    evt.Skip();
}

void main::populate_project_history(bool init) {
    if (init) {
        lbi = std::vector<customListBoxItem>();
        lbi.clear();
    }
    
    wxTextFile project_history_file;
    std::string appdata_path = std::getenv("APPDATA");
    appdata_path.erase(appdata_path.size() - 8);
    appdata_path += "\\Local";
    std::string minipy_subpath = "\\MiniPy";
    std::string filename = "\\hist.txt";
    wxString dir_path = appdata_path + minipy_subpath;
    wxString file_path = appdata_path + minipy_subpath + filename;
    wxDir dir;
    if (!dir.Exists(dir_path)) {
        dir.Make(dir_path);
    }
    if (!wxFileExists(file_path)) {
        wxTextFile dummy;
        dummy.Create(file_path);
    }
    project_history_file.Open(file_path);
    wxString str;
    wxString name;
    wxString path;
    int counter = 0;
    for (str = project_history_file.GetFirstLine(); !project_history_file.Eof(); str = project_history_file.GetNextLine()) {
        if (counter % 2 == 0) {
            name = str;
        }
        else {
            path = str;
        }
        if (counter % 2 == 1 && counter != 0) {
            customListBoxItem item(name, path);
            lbi.push_back(item);
        }
        counter += 1;
    }

    project_history->Clear();
    for (int i = 0; i < (int) lbi.size(); i++) {
        wxString name = lbi[i].get_name();
        project_history->AppendAndEnsureVisible(name);
    }
}

//Switch for seperating events by the ID of the menu item that caused them
void main::OnMenuEvent(wxCommandEvent& evt) {
	switch (evt.GetId()) {
	case ID_MENU_RUN:
        status_bar->PushStatusText("Running Script", 0);
		this->event_run(evt);
        status_bar->PopStatusText();
		break;
    case ID_MENU_LOAD:
        status_bar->PushStatusText("Loading File", 0);
        this->event_load(evt);
        status_bar->PopStatusText();
        break;
    case ID_MENU_SAVE:
        status_bar->PushStatusText("Saving File", 0);
        this->event_save(evt);
        status_bar->PopStatusText();
        break;
	case wxID_EXIT:
        status_bar->PushStatusText("Exiting", 0);
		this->event_exit(evt);
        status_bar->PopStatusText();
		break;
    case ID_MENU_OPEN_PROJ:
        status_bar->PushStatusText("Opening project", 0);
        this->event_project_load(evt);
        status_bar->PopStatusText();
        break;
    case ID_MENU_SAVE_PROJ:
        status_bar->PushStatusText("Saving Project", 0);
        this->event_project_save(evt);
        status_bar->PopStatusText();
        break;
    case ID_MENU_CLOSE_PROJ:
        status_bar->PushStatusText("Closing Projct", 0);
        this->event_project_close(evt);
        status_bar->PopStatusText();
        break;
	default:
		break;
	}
}

void main::event_project_load(wxCommandEvent &evt) {
    current_proj = new proj(this, tree_list, code_control, false, wxString());
    bool extant = false;
    wxString root = current_proj->get_root_dir();
    root.append("\\");
    //Existence check broken - Doesn't add proj from new directory to list
    for (int i = 0; i < (int)lbi.size(); i++) {
        wxString local_root = lbi[i].get_root_dir();
        if (local_root == root) {
            extant = true;
            break;
        }
    }
    if (!extant) {
        wxTextEntryDialog dlg(this, "What is the name of this project?", "", wxEmptyString, wxOK | wxCENTRE | wxWS_EX_VALIDATE_RECURSIVELY);
        dlg.ShowModal();
        wxString name = dlg.GetValue();
        customListBoxItem new_item(name, root);
        lbi.push_back(new_item);
    }
    populate_project_history(false);
    evt.Skip();
}

void main::event_project_save(wxCommandEvent &evt) {
    if (current_proj != nullptr) {
        current_proj->save_current_file();
    }
    evt.Skip();
}

void main::event_project_close(wxCommandEvent &evt) {
    delete current_proj;
    current_proj = nullptr;
    code_control->ClearAll();
    tree_list->DeleteAllItems();
    tree_list->AppendItem(tree_list->GetRootItem(), "<local>");
    evt.Skip();
}

void main::event_exit(wxCommandEvent &evt) {
    this->Destroy();
	evt.Skip();
}

void main::event_run(wxCommandEvent &evt) {
    std::string command = "python -c ";
    //Note: This means no " in code, only '. TODO: Fix this
    command += '"';

    std::string script;
    for (int curr_line = 0; curr_line < code_control->GetLineCount(); curr_line++) {
        wxString line = code_control->GetLine(curr_line);
        std::string std_line = line.ToStdString();
        if (std_line.size() != 0) {
            while (std_line[std_line.size() - 1] == '\n' or std_line[std_line.size() - 1] == '\r') {
                std_line.pop_back();
            }
            if (&std_line.back() != ":" && &std_line.back() != ";") {
                std_line += ";";
            }
            script += std_line;
        }
    }
    command += script;

    command += '"';
    FILE* pipe = _popen(command.c_str(), "r");
    if (pipe == NULL) {
        wxMessageDialog script_error_dialog(this, "Piping Error", "", wxOK | wxICON_ERROR | wxSTAY_ON_TOP);
        script_error_dialog.ShowModal();
        return;
    }
    char buffer[128];
    std::string result = "";
    while (fgets(buffer, 128, pipe)) {
        result.append(buffer);
    }
    pseudo_console->AppendText(result);
    pseudo_console->AppendText("----------\n");
    _pclose(pipe);
    evt.Skip();
}

void main::event_load(wxCommandEvent &evt) {
    //Create and use File load Dialog
    wxFileDialog openFileDialog(this, _T("Load Python File"), "", "", "Python Files (*.py) |*.py", wxFD_OPEN|wxFD_FILE_MUST_EXIST);
    if (openFileDialog.ShowModal() == wxID_CANCEL) {
        return;
    }
    wxFileInputStream input_stream(openFileDialog.GetPath());
    if (!input_stream.IsOk()) {
        wxLogError("Cannot open file '%s'.", openFileDialog.GetPath());
        return;
    }
    code_control->ClearAll();
    wxString content;
    int buffer_size = 1024;
    char* buffer = new char[buffer_size];
    do {
        input_stream.Read(buffer, buffer_size);
        wxString convBuffer = _(buffer);
        while (convBuffer.size() > 1024) {
            //For some reason, the conversion adds 4 unknown chars.
            convBuffer.RemoveLast();
        }
        content.Append(convBuffer);
    } while (input_stream.LastRead() > 0);
    code_control->SetText(content);
    delete [] buffer;
    evt.Skip();
}

void main::event_save(wxCommandEvent& evt) {
    //Create and use File Dialog
    wxFileDialog saveFileDialog(this, _T("Save Python File"), "", "", "Python Files (*.py) |*.py", wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
    if (saveFileDialog.ShowModal() == wxID_CANCEL) {
        return;
    }
    wxTextFile outFile;
    outFile.Open(saveFileDialog.GetPath());

    outFile.Clear();
    wxString content = code_control->GetText();
    //Technically adds all the text as one line, but since that contains '\n', this works
    outFile.AddLine(content);

    outFile.Write();
    evt.Skip();
}
