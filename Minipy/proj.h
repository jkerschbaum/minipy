#pragma once

#include "wx/dirdlg.h"
#include "wx/dir.h"
#include "wx/arrstr.h"
#include "wx/treelist.h"
#include "wx/stc/stc.h"
#include "wx/wfstream.h"
#include "wx/textfile.h"
#include <sstream>
#include <vector>

#include "tlcComp.h"
#include "ID.h"

class proj
{
public:
	proj(wxWindow*, wxTreeListCtrl*, wxStyledTextCtrl*, bool, wxString);
	~proj();

public:

	wxArrayString files;
	wxString root_dir = wxEmptyString;
	wxStyledTextCtrl* stc_ptr = nullptr;
	wxTreeListCtrl* tlc_ptr = nullptr;
	wxString current_file = wxEmptyString;
	tlcComp* comp = nullptr;

public:
	void get_root_dir(wxWindow*);
	void get_files();
	void filter();
	wxArrayString split_path(wxString, bool);
	void set_tree_list(wxTreeListCtrl*);
	bool has_root_dir();
	void treelist_event_handler(wxTreeListEvent&);
	void save_current_file();
	wxString get_root_dir();
};

