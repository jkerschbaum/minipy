#include "tlcComp.h"

tlcComp::tlcComp() {

}

tlcComp::~tlcComp() {

}

int tlcComp::Compare(wxTreeListCtrl* treelist, unsigned column, wxTreeListItem first, wxTreeListItem second) {
	bool first_has_child = treelist->GetFirstChild(first).IsOk();
	bool second_has_child = treelist->GetFirstChild(second).IsOk();
	if (first_has_child == second_has_child) {
		std::vector<std::string> vec;
		vec.push_back(treelist->GetItemText(first).ToStdString());
		vec.push_back(treelist->GetItemText(second).ToStdString());
		sort(vec.begin(), vec.end());
		if (vec[0] == treelist->GetItemText(first).ToStdString()) {
			return -1;
		}
		return 1;
	}
	if (first_has_child) {
		return -1;
	}
	return 1;
}
