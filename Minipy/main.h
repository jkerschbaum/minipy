#pragma once

#include "wx/wx.h"
#include "wx/stc/stc.h"
#include "wx/menu.h"
#include "wx/filedlg.h"
#include "wx/wfstream.h"
#include "wx/log.h"
#include "wx/textfile.h"
#include "wx/statusbr.h"
#include "wx/msgdlg.h"
#include "wx/treelist.h"
#include "wx/sizer.h"
#include "wx/textctrl.h"
#include "wx/listbox.h"
#include "wx/textdlg.h"
#include "wx/filefn.h"
#include "wx/dir.h"
#include "proj.h"
#include "customListBoxItem.h"
#include <vector>
#include <cstdio>

#include "ID.h"

class main : public wxFrame
{
public:
	main();
	~main();

public:
	wxMenuBar* menu_bar = nullptr;
	wxMenu* main_menu = nullptr;
	wxMenu* project_menu = nullptr;
	wxStyledTextCtrl* code_control = nullptr;
	wxStatusBar* status_bar = nullptr;
	wxTreeListCtrl* tree_list = nullptr;
	wxTextCtrl* pseudo_console = nullptr;
	wxListBox* project_history = nullptr;
	wxFlexGridSizer* sizer = nullptr;
	std::vector<customListBoxItem> lbi;
	proj* current_proj = nullptr;

public:
	void event_run(wxCommandEvent&);
	void event_exit(wxCommandEvent&);
	void event_load(wxCommandEvent&);
	void event_save(wxCommandEvent&);
	void event_project_load(wxCommandEvent&);
	void event_project_save(wxCommandEvent&);
	void event_project_close(wxCommandEvent&);
	void OnMenuEvent(wxCommandEvent&);
	void setStyle(wxStyledTextCtrl&);
	void OnMarginClick(wxStyledTextEvent&);
	void populate_project_history(bool);
	void OnListBoxEvent(wxCommandEvent&);
	void OnConsoleCommandEvent(wxCommandEvent&);
};

