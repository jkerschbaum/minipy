#include "proj.h"

proj::proj(wxWindow* parent, wxTreeListCtrl* tlc, wxStyledTextCtrl* stc, bool from_history = false, wxString history_dir = wxString()) {
	if (from_history) {
		history_dir.RemoveLast();
		root_dir = history_dir;
	}
	else {
		get_root_dir(parent);
	}
    if (!(has_root_dir())) {
        //No Root
		//TODO: Handle this better
        return;
    }
	stc_ptr = stc;
	tlc_ptr = tlc;
    get_files();
	filter();
	set_tree_list(tlc);
	comp = new tlcComp();
	tlc->SetItemComparator(comp);
	tlc->SetSortColumn(0);
	tlc->Bind(wxEVT_TREELIST_ITEM_ACTIVATED, &proj::treelist_event_handler, this);
}

proj::~proj() {
	tlc_ptr->Unbind(wxEVT_TREELIST_ITEM_ACTIVATED, &proj::treelist_event_handler, this);
	delete comp;
}

void proj::treelist_event_handler(wxTreeListEvent &evt) {
	//Save current file
	if (current_file != "") {
		wxTextFile outFile;
		outFile.Open(current_file);

		outFile.Clear();
		wxString content = stc_ptr->GetText();
		//Technically adds all the text as one line, but since that contains '\n', this works
		outFile.AddLine(content);

		outFile.Write();
	}

	wxTreeListItem item = evt.GetItem();
	if (tlc_ptr->GetFirstChild(item).IsOk()) {
		//Child exists -> Folder
		evt.Skip();
		return;
	}
	wxString name = tlc_ptr->GetItemText(item);
	wxString path;
	for (int i = 0; i < (int)files.size(); i++) {
		if (files[i].EndsWith(name)) {
			//File found
			path = files[i];
			break;
		}
	}

	//Load new File
	wxFileInputStream input_stream(path);
	if (!input_stream.IsOk()) {
		return;
	}
	stc_ptr->ClearAll();
	wxString content;
	int buffer_size = 1024;
	char* buffer = new char[buffer_size];
	do {
		input_stream.Read(buffer, buffer_size);
		wxString convBuffer = _(buffer);
		while (convBuffer.size() > 1024) {
			//For some reason, the conversion adds 4 unknown chars.
			convBuffer.RemoveLast();
		}
		content.Append(convBuffer);
	} while (input_stream.LastRead() > 0);
	stc_ptr->SetText(content);
	current_file = path;
	delete[] buffer;

	evt.Skip();
}

void proj::get_root_dir(wxWindow* parent) {
	wxDirDialog dlg(parent, "Root Dir", "", wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER);
	int result = dlg.ShowModal();
	if (!(result == wxID_OK)) {
		return;
	}
	wxString path_res = dlg.GetPath();
	root_dir = path_res;
}

bool proj::has_root_dir() {
	return root_dir != wxEmptyString;
}

void proj::get_files() {
	wxArrayString files_result = wxArrayString();
	size_t result = wxDir::GetAllFiles(root_dir, &files_result, wxEmptyString, wxDIR_FILES | wxDIR_DIRS);
	if (result == 0) {
		return;
	}
	files = files_result;
}

void proj::filter() {
	wxArrayString new_files;
	for (int i = 0; i < (int)files.size(); i++) {
		//TODO: Filter out more types?
		if (files[i].ends_with(".pyc")) {
			continue;
		}
		new_files.Add(files[i]);
	}
	files = new_files;
}

wxArrayString proj::split_path(wxString in, bool is_root) {
	if (!is_root) {
		in = in.Right(in.size() - root_dir.size());
	}
	std::string std_in = in.ToStdString();
	std::string t = "";
	std::stringstream x(std_in);
	wxArrayString result;
	std::vector<std::string> vec;
	while (std::getline(x, t, '\\')){
		vec.push_back(t);
	}
	if (vec.size() > 0) {
		vec.erase(vec.begin());
	}
	for (int i = 0; i < (int)vec.size(); i++) {
		result.Add(wxString(vec[i]));
	}
	return result;
}

void proj::set_tree_list(wxTreeListCtrl* tlc) {
	tlc->DeleteAllItems();
	wxArrayString root_item_array = split_path(root_dir, true);
	tlc->AppendItem(tlc->GetRootItem(), root_item_array.Last());
	wxTreeListItem root_item = tlc->GetFirstChild(tlc->GetRootItem());
	wxTreeListItem trav_item;
	for (int i = 0; i < (int)files.GetCount(); i++) {
		trav_item = wxTreeListItem(root_item);
		wxArrayString arr = split_path(files[i], false);
		for (int j = 0; j < (int) arr.GetCount(); j++) {
			wxTreeListItem candidate = tlc->GetFirstChild(trav_item);
			if (candidate.IsOk()) {
				//First child exists

				bool does_exist = false;
				while (candidate.IsOk()) {
					if (tlc->GetItemText(candidate).IsSameAs(arr[j], true)) {
						trav_item = candidate;
						does_exist = true;
						break;
					}
					else {
						candidate = tlc->GetNextSibling(candidate);
					}
				}
				if (!does_exist) {
					//If we arrive here, then a child to trav_item with the proper name does not exist.
					tlc->AppendItem(trav_item, arr[j]);
					wxTreeListItem item = tlc->GetFirstChild(trav_item);
					while (tlc->GetNextSibling(item).IsOk()) {
						item = tlc->GetNextSibling(item);
					}
					trav_item = item;
				}
			}
			else {
				//No first child
				tlc->AppendItem(trav_item, arr[j]);
				trav_item = tlc->GetFirstChild(trav_item);
			}
		}
	}
}

void proj::save_current_file() {
	if (current_file != "") {
		wxTextFile outFile;
		outFile.Open(current_file);

		outFile.Clear();
		wxString content = stc_ptr->GetText();
		//Technically adds all the text as one line, but since that contains '\n', this works
		outFile.AddLine(content);

		outFile.Write();
	}
}

wxString proj::get_root_dir() {
	return root_dir;
}
