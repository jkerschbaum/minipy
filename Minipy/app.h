#pragma once

#include "wx/wx.h"
#include "main.h"

class app : public wxApp
{
public:

	app();
	~app();

private:
	main* main_frame = nullptr;

public:
	virtual bool OnInit();
	virtual int OnExit();
};

