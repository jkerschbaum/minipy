#pragma once

#include "wx/treelist.h"
#include <vector>

class tlcComp : public wxTreeListItemComparator
{
public:
	tlcComp();
	~tlcComp();
	int Compare(wxTreeListCtrl*, unsigned, wxTreeListItem, wxTreeListItem);
};

