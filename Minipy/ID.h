#pragma once

//Define IDs

#define ID_TREELIST			501
#define ID_STC				502
#define ID_STATUS			503

#define ID_MENU_RUN			1001
#define ID_MENU_LOAD		1002
#define ID_MENU_SAVE		1003

#define ID_MENU_OPEN_PROJ	2001
#define ID_MENU_SAVE_PROJ	2002
#define ID_MENU_CLOSE_PROJ	2003

//Define Command IDs

#define COMM_NONE			10000
#define COMM_RUN			10001
#define COMM_DELETE_LAST	10002
#define COMM_CLEAR			10003
