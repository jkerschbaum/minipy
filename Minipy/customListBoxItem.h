#pragma once

#include "wx\wx.h"

class customListBoxItem
{
public:
	customListBoxItem(wxString, wxString);
	~customListBoxItem();
public:
	wxString name = wxEmptyString;
	wxString root_dir = wxEmptyString;
public:
	wxString get_name();
	wxString get_root_dir();
	void set_name(wxString);
};

